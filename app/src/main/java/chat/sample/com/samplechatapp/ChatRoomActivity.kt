package chat.sample.com.samplechatapp

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ChildEventListener


class ChatRoomActivity : AppCompatActivity() {


    private lateinit var btnSendMsg: Button
    private lateinit var inputMsg: EditText
    private lateinit var chatConversation: TextView

    private lateinit var userName: String
    private lateinit var roomName: String
    private lateinit var root: DatabaseReference
    private lateinit var tempKey: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.chat_room)

        btnSendMsg = findViewById(R.id.btn_send)
        inputMsg = findViewById(R.id.msg_input)
        chatConversation = findViewById(R.id.textView)

        userName = intent.extras.get("user_name").toString()
        roomName = intent.extras.get("room_name").toString()
        title = " Room - $roomName"

        root = FirebaseDatabase.getInstance().reference.child("Chat").child(roomName);

        btnSendMsg.setOnClickListener {

            val map = HashMap<String, Any>()
            tempKey = root.push().key!!
            root.updateChildren(map)

            val messageRoot = root.child(tempKey)
            val map2 = HashMap<String, Any>()
            map2["name"] = userName
            map2["msg"] = inputMsg.text.toString()

            messageRoot.updateChildren(map2)
            inputMsg.setText("")
        }

        root.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {

                appendChatConversation(dataSnapshot)
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, s: String?) {

                appendChatConversation(dataSnapshot)

            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {

            }

            override fun onChildMoved(dataSnapshot: DataSnapshot, s: String?) {

            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })

    }

    private var chatMsg: String? = null
    private var chatUserName: String? = null

    private fun appendChatConversation(dataSnapshot: DataSnapshot) {

        val i = dataSnapshot.children.iterator();

        while (i.hasNext()) {

            chatMsg = (i.next() as DataSnapshot).value as String?
            chatUserName = (i.next() as DataSnapshot).value as String?

            chatConversation.append("$chatUserName : $chatMsg \n")
        }

    }
}