package chat.sample.com.samplechatapp

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AlertDialog
import com.google.firebase.database.*
import com.google.firebase.database.DataSnapshot
import android.widget.TextView
import android.content.Intent
import android.view.View
import android.widget.AdapterView
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private lateinit var addRoom: Button
    private lateinit var roomName: EditText

    private lateinit var listView: ListView
    private lateinit var arrayAdapter: ArrayAdapter<String>
    private var arrayList: ArrayList<String> = arrayListOf()
    private lateinit var name: String
    private var root: DatabaseReference = FirebaseDatabase.getInstance().reference.root

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addRoom = findViewById(R.id.btn_add_room)
        roomName = findViewById(R.id.room_name_edittext)
        listView = findViewById(R.id.listView)

        arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList)
        listView.adapter = arrayAdapter
        requestUserName()

        addRoom.setOnClickListener {
            val map: HashMap<String, Any> = hashMapOf()
            map[roomName.text.toString()] = ""
            root.child("Chat").updateChildren(map)
        }

        btn_search_user.setOnClickListener {
            searchUser(username_edittext.text.toString())
        }
        root.child("Chat").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(p0: DataSnapshot) {
                val set = HashSet<String>()
                val i = p0.children.iterator()

                while (i.hasNext()) {
                    set.add((i.next() as DataSnapshot).key!!)
                }

                arrayList.clear()
                arrayList.addAll(set)

                arrayAdapter.notifyDataSetChanged()

            }

        })
        listView.onItemClickListener = AdapterView.OnItemClickListener { _, view, i, l ->
            val intent = Intent(applicationContext, ChatRoomActivity::class.java)
            intent.putExtra("room_name", (view as TextView).text.toString())
            intent.putExtra("user_name", name)
            startActivity(intent)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun requestUserName() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Enter name:")

        val inputField = EditText(this)

        builder.setView(inputField)
        builder.setPositiveButton("OK") { _, _ ->

            name = inputField.text.toString()
            val userRoot = root.child("User")
            val tempKey = userRoot.push().key!!
            val map = HashMap<String, Any>()
            map["username"] = name
            map["user_tag"] = "@$name"
            userRoot.child(tempKey).updateChildren(map)

        }

        builder.setNegativeButton("Cancel") { dialogInterface, _ ->
            dialogInterface.cancel()
            requestUserName()
        }

        builder.show()
    }

    private fun searchUser(name: String) {

        val set = HashSet<String>()

        val res = root.child("User")

        res.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {

                val i = p0.children.iterator()

                while (i.hasNext()) {
                    if (((i.next() as DataSnapshot).key!!) === name) {
                        set.add((i.next() as DataSnapshot).key!!)
                        arrayAdapter.clear()
                        arrayAdapter.addAll(set)
                        arrayAdapter.notifyDataSetChanged()
                    }
                }
            }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildRemoved(p0: DataSnapshot) {
            }

        })
    }
}
